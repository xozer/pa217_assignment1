﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;

public class ArrivalGameObject : AbstractSteeringGameObject
{
    public float targetRadius;
    public float slowRadius;
    public float maxAcceleration;

    [SerializeField]
    protected GameObject objectToFollow;
    protected float timeToTarget = 0.1f;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();

        // TODO Task 1 Arrival 
        //      Add your code here (the object to follow is determined by the value of "objectToFollow" variable).
        //      Set the final velocity to "Velocity" property. The maximum speed of the agent is determined by "maxSpeed".
        //      If you want to change the rotation of the agent, you can use, for example, "LookDirection" property.
        //      In case you would prefer to modify the transform.position directly, you can change the movementControl to Manual (see AbstractSteeringGameObject class for info).
        //      Feel free to extend the codebase. However, make sure it is easy to find your solution.

        Vector3 direction = objectToFollow.transform.position - transform.position;
        float distance = direction.magnitude;

        if (distance < targetRadius)
        {
            Velocity = Vector3.zero;
            return;
        }

        float targetSpeed = 0;
        if (distance > slowRadius)
            targetSpeed = maxSpeed;
        else
            targetSpeed = maxSpeed * distance / slowRadius;

        Vector3 targetVelocity = direction.normalized * targetSpeed;
        Vector3 steering = (targetVelocity - Velocity) / timeToTarget;
        if (steering.magnitude > maxAcceleration)
            steering = steering.normalized * maxAcceleration;

        Velocity += steering;

        LookDirection = direction;
    }

    protected override void LateUpdate()
    {
        base.LateUpdate();
    }
}
