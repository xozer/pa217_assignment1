﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockingGameObject : AbstractSteeringGameObject
{
    [SerializeField]
    protected float neighbourRadius = 1.0f;

    public FlockSpawner Spawner { get; set; }

    private Vector3 initialVelocity;

    protected override void Start()
    {
        base.Start();
        
        Velocity = new Vector3(Random.insideUnitSphere.x, 0.0f, Random.insideUnitSphere.y).normalized * maxSpeed;
        initialVelocity = Velocity;
    }

    protected override void Update()
    {
        base.Update();
        LookDirection = Velocity.normalized;

        // TODO Task 3 Flocking
        //      Use methods below to compute individual steering behaviors.
        //      Information about other agents is stored in "Spawner.Agents".
        //      The variable "neighbourRadius" holds the radius which should be used for neighbour detection.
        //      Set the final velocity to "Velocity" property. The maximum speed of the agent is determined by "maxSpeed".
        //      In case you would prefer to modify the transform.position directly, you can change the movementControl to Manual (see AbstractSteeringGameObject class for info).
        //      Feel free to extend the codebase. However, make sure it is easy to find your solution.
        List<FlockingGameObject> neighbours = FindNeighbours();
        Vector3 flocking = Spawner.cohesionWeight * ComputeCohesion(neighbours) + Spawner.allignmentWeight * ComputeAlignment(neighbours) + Spawner.seperationWeight * ComputeSeparation(neighbours);

        if (flocking.magnitude > maxSpeed)   // making sure that the unit speed does not pass maximum speed
            flocking = flocking.normalized * maxSpeed;

        Velocity = flocking.magnitude > 0 ? flocking : initialVelocity; // if has neighbours that effect the behavior -> flocking, otherwise initial velocity
    }

    protected override void LateUpdate()
    {
        base.LateUpdate();
    }

    protected Vector3 ComputeSeparation(List<FlockingGameObject> neighbours)
    {
        // TODO Task 3 Flocking – separation
        if (neighbours.Count <= 0)
            return Vector3.zero;

        Vector3 seperation = Vector3.zero;
        foreach (FlockingGameObject fgo in neighbours)
            seperation += transform.position - fgo.transform.position;
        return seperation / neighbours.Count;
    }

    protected Vector3 ComputeAlignment(List<FlockingGameObject> neighbours)
    {
        // TODO Task 3 Flocking – alignment
        if (neighbours.Count <= 0)
            return transform.forward;

        Vector3 alignment = Vector3.zero;
        foreach (FlockingGameObject fgo in neighbours)
            alignment += fgo.transform.forward;
        return alignment / neighbours.Count;
    }

    protected Vector3 ComputeCohesion(List<FlockingGameObject> neighbours)
    {
        // TODO Task 3 Flocking – cohesion
        if(neighbours.Count <= 0)
            return Vector3.zero;

        Vector3 cohesion = Vector3.zero;
        float distanceDifference = 0f;
        foreach (FlockingGameObject fgo in neighbours)
        {
            distanceDifference = (new Vector2(fgo.transform.position.x, fgo.transform.position.z) - new Vector2(transform.position.x, transform.position.z)).magnitude;
            cohesion += fgo.transform.position;
        }
        return cohesion / neighbours.Count - transform.position;
    }

    public override void SetDebugObjectsState(bool newState)
    {
        base.SetDebugObjectsState(newState);
    }

    private List<FlockingGameObject> FindNeighbours()
    {
        List<FlockingGameObject> neighbours = new List<FlockingGameObject>();
        foreach(FlockingGameObject fgo in Spawner.Agents)
        {
            if((new Vector2(fgo.transform.position.x, fgo.transform.position.z) - new Vector2(transform.position.x, transform.position.z)).magnitude < neighbourRadius)
                neighbours.Add(fgo);
        }
        return neighbours;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, neighbourRadius);
    }
}
